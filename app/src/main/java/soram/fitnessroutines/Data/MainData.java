package soram.fitnessroutines.Data;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import soram.fitnessroutines.MainActivity;
import soram.fitnessroutines.R;
import soram.fitnessroutines.Routines.RoutinesModel;
import soram.fitnessroutines.Routines.SC.SCModel;
import soram.fitnessroutines.TM.ListModel;

public class MainData {
    int week;

    public static final String[] routineList = {"PPL", "5/3/1", "GZCLP", "Upper/Lower", "StrongLift 5x5"};
    public ArrayList<String> repPercentages () {
        return new ArrayList<>(Arrays.asList("One rep max: ", "95% of 1RM: ", "90% of 1RM: ", "85% of 1RM: ", "80% of 1RM: ", "75% of 1RM: ", "70% of 1RM: ",
                "65% of 1RM: ", "60% of 1RM: ", "55% of 1RM: ", "50% of 1RM: "));
    }

    public static final ArrayList<String> activityLevel = new ArrayList<>(Arrays.asList("Sedentary: Little or no exercise, spend most of the day sitting(Office job).",
            "Lightly active: Light exercise, sports 1 to 3 days a week / spend a good time of your day on your feet (Teacher, Salesman)",
            "Active: Sports 3 to 5 days a week / spend a good time of your day doing physical activity (Waitress, Mailman)",
            "Very active: Heavy exercise, sports 6 to 7 days a week / spend most of the day doing deavy physical activity (Builder, Carpenter)"));

    public static final ArrayList<RoutinesModel> getRoutineList = new ArrayList<>(Arrays.asList(
            new RoutinesModel("Strong Curves", "Beginner / Ladies", "2-4 days / week", 0xfff956ff),
            new RoutinesModel("Phraks Greyskull LP", "Beginner","3 days / week", 0xff03be4b),
            new RoutinesModel("Ice Cream Fitness 5x5", "Beginner","3 days / week", 0xff03be4b),
            new RoutinesModel("Push/Pull/Legs", "Intermediate", "6 days / week",0xffFFBB00)



    ));
//    new RoutinesModel("Push/Pull/Legs", "Intermediate", 0xFFFFBB00)
    public static final ArrayList<String> getLiftNames = new ArrayList<>(Arrays.asList(
                "Bench press",
                "Barbell row",
                "Chinup",
                "Squat",
                "Overhead press",
                "Deadlift"
        ));

    public static final String getGreySkullInfo = "It is best thought of as an introductory lifting program to build familiarity" +
            " and comfortability with the fundamental barbell movements that are a core component of most strength training routines. " +
            "For this reason, we generally recommend this program be run entirely as-is without much " +
            "modification, and only for a maximum of 3 months.\n"+
            "\n" +
            "If you absolutely have no idea where to start, begin with the bar and do 5 reps. " +
            "From there, add weight incrementally based on the perceived difficulty until your rep speed " +
            "slows significantly from the previous set. That is your starting weight. It is heavy enough to drive " +
            "the adaptations we want (i.e. strength, neuromuscular coordination, hypertrophy, etc)" +
            " while being light enough to ensure sustainable progress.\n"+
            "\n" +
            "Rest 2-3 minutes between each set.\n"+
            "The \"+\" denotes that last set for all lifts is AMRAP - As Many Reps As Possible. " +

            "Note that this is should not be to true muscular failure, and you should leave 1-2 reps \"in the tank\" on these sets\n"+
            "\n" +
            "Add 2.5 pounds(1.25kg) to all upper-body lifts between workouts.\n"+
            "Add 5 pounds(2.5kg) to all lower-body lifts between workouts.\n"+
            "\n" +
            "If you reach more than 10 reps on your AMRAP set, you may double the weight added.\n"+
            "\n" +
            "If you fail to perform at least 15 reps combined across all sets, deload that lift by 10%.\n"+
            "\n" +
            "If you cannot do chin ups, do negatives instead until you can.\n"+
            "\n" +
            "More info at https://old.reddit.com/r/Fitness/wiki/phraks-gslp"
            ;

    public static final String getIceCreamInfo = "A potent and proven novice muscle and strength " +
            "building program from Jason Blaha & Ice Cream Fitness.\n"+
            "\n" +
            "The program is to be conducted 3 days per week alternating workout A&B on non-consecutive days.\n"+
            "\n" +
            "Break times between sets are 3-5 minutes for the 5x5 sets and 1-2 minutes for the 3x8 sets\n"+
            "\n" +
            "Add 5 lbs (2.5 kg) per workout on main lifts, on accessories add weight once you finish all sets/reps.\n"+
            "\n" +
            "If you fail a rep/set/whatever, you repeat that weight during the next workout. " +
            "\n" +
            "If you fail the next workout, you lower your weight by 10% and continue" +
            " to use that until progress. Reset weights get rounded down\n"+
            "\n" +
            "More info at https://www.muscleandstrength.com/workouts/jason-blaha-ice-cream-fitness-5x5-novice-workout"

            ;

    public static final String getStrongCurvesInfo = "Strong Curves (SC) is an exercise and nutrition program developed by Bret Contreras." +
            " SC places significantly emphasis on the gluteal muscles, and is considered a great introduction for" +
            " women into lifting weights.\n" +
            "The program is scalable, can be tailored to the liking and ability of" +
            " the user and even offers an at-home alternative for those without access to a gym."+
            "\n" +
            "Beginners should follow an ABAC format, which makes your schedule look like this: A, B, rest, A, C, rest, rest.\n"+
            "\n" +
            "Exercises with same color should be superesetted, that means doing one set of an exercise followed" +
            " immediately with one set of exercise with same color, rest 30-90 seconds and repeat untill you finish all your sets.\n"+
            "\n" +
            "es = each side, sec = seconds.\n"+
            "\n" +
            "More info at https://www.reddit.com/r/StrongCurves/wiki/index"

            ;

    public static final String getTDEEInfo = "To gain weight eat at 250-500 kcal surplus above your calculated TDEE.\n"+
            "To lose weight eat at 250-500 kcal deficit under your calculated TDEE."

            ;

    public static final String getPPLInfo = "It's 6 days a week. You can run it one of two ways: PPLRPPL or PPLPPLR (where R denotes a rest day) " +
            "depending on your schedule and preferences: it really makes no" +
            " difference. Personally, I would run the program in the Pull, Push, Legs order."+
            "\n" +
            "Progression:\n"+
            "2.5kg/5lbs for upper body lifts (bench press, row, overhead press)\n" +

            "2.5kg/5lbs for squats\n" +

            "5kg/10lbs for deadlifts\n"+
            "\n" +
            "So, the first movements are done for sets of 5, and the final " +
            "set is what's known as an AMRAP set (As Many Reps As Possible). This doesn't mean that" +
            " you can just YOLO it and push until you can't push anymore. By As Many Reps As Possible, it really" +
            " means As many reps as possible while still maintaining good form."+
            "\n" +
            "Rest as long as is needed between sets. For a general guideline, I would recommend:\n" +

            "3-5 minutes between your first exercise of the day.\n" +

            "1-3 minutes between all your other exercises.\n"+
            "\n" +
            "If you fail a session 3 times in a row (for example, if you fail to hit 3x5 on squats at 100kg 3 times in a row), you need to lower the weight, deload by 10%"+
            "\n" +
            "More info https://www.reddit.com/r/Fitness/comments/37ylk5/a_linear_progression_based_ppl_program_for/"

            ;

    public static final ArrayList<SCModel> getWeekOneA = new ArrayList<>(Arrays.asList(
            new SCModel("EXERCISE", "SETS", "REPS", 0xFFf45942),
            new SCModel("Bodyweight Glute Bridge", "3", "10-20", 0xFFFFBB00),
            new SCModel("One Arm Dumbbell Row", "3", "8-12", 0xFFFFBB00),
            new SCModel("Bodyweight Box Squat", "3", "10-20", 0xFF609dff),
            new SCModel("Barbell Bench Press", "3", "8-12", 0xFF609dff),
            new SCModel("Dumbbell Romanian Deadlift", "3", "10-20", 0xFFff60c4),
            new SCModel("Side Lying Abduction", "1", "15-30 es", 0xFFf74cb8),
            new SCModel("Front Plank", "1", "20-120 sec", 0xFFf42c6b),
            new SCModel("Side Plank", "1", "20-60 sec", 0xFFff0c3d)

    ));

    public static final ArrayList<SCModel> getWeekOneB = new ArrayList<>(Arrays.asList(
            new SCModel("EXERCISE", "SETS", "REPS", 0xFFf45942),
            new SCModel("Bodyweight Foot Elevated Single Leg Glute Bridge", "3", "10-20", 0xFFFFBB00),
            new SCModel("Front Lat Pulldown", "3", "8-12", 0xFFFFBB00),
            new SCModel("Bodyweight Step Up", "3", "10-20 es", 0xFF609dff),
            new SCModel("Dumbbell Military Press", "3", "8-12", 0xFF609dff),
            new SCModel("Bodyweight 45 degree Back Extension", "3", "10-20", 0xFFff60c4),
            new SCModel("Side Lying Clam", "1", "15-30 es", 0xFFf74cb8),
            new SCModel("Crunch", "1", "15-30", 0xFFf42c6b),
            new SCModel("Side Crunch", "1", "15-30 es", 0xFFff0c3d)

    ));

    public static final ArrayList<SCModel> getWeekOneC = new ArrayList<>(Arrays.asList(
            new SCModel("EXERCISE", "SETS", "REPS", 0xFFf45942),
            new SCModel("Glute March", "3", "60 sec", 0xFFFFBB00),
            new SCModel("Seated Row", "3", "8-12", 0xFFFFBB00),
            new SCModel("Bodyweight Parallel Squat", "3", "10-20", 0xFF609dff),
            new SCModel("Dumbbell Incline Press", "3", "8-12", 0xFF609dff),
            new SCModel("Single Leg Romanian Deadlift", "3", "10-20 es", 0xFFff60c4),
            new SCModel("X-Band Walk (Light)", "1", "10-20 es", 0xFFf74cb8),
            new SCModel("RKC Plank", "1", "10-30 sec", 0xFFf42c6b),
            new SCModel("Rope Horizontal Chop", "1", "10 es", 0xFFff0c3d)

    ));

    public static final ArrayList<SCModel> getWeekTwoA = new ArrayList<>(Arrays.asList(
            new SCModel("EXERCISE", "SETS", "REPS", 0xFFf45942),
            new SCModel("Bodyweight Hip Thrust", "3", "10-20", 0xFFFFBB00),
            new SCModel("Standing Cable Row", "3", "8-12", 0xFFFFBB00),
            new SCModel("Goblet Squat", "3", "10-20", 0xFF609dff),
            new SCModel("Barbell Bench Press", "3", "8-12", 0xFF609dff),
            new SCModel("Barbell Romanian Deadlift", "3", "10-20", 0xFFff60c4),
            new SCModel("Side Lying Abduction", "1", "15-30 es", 0xFFf74cb8),
            new SCModel("Feet Elevated Plank", "1", "20-60 sec", 0xFFf42c6b),
            new SCModel("Side Plank", "1", "20-60 sec", 0xFFff0c3d)

    ));

    public static final ArrayList<SCModel> getWeekTwoB = new ArrayList<>(Arrays.asList(
            new SCModel("EXERCISE", "SETS", "REPS", 0xFFf45942),
            new SCModel("Bodyweight Single Leg Glute Bridge", "3", "10-20 es", 0xFFFFBB00),
            new SCModel("Negative Chin Up", "3", "3", 0xFFFFBB00),
            new SCModel("Bodyweight Walking Lunge", "3", "20-40", 0xFF609dff),
            new SCModel("Dumbbell Military Press", "3", "8-12", 0xFF609dff),
            new SCModel("Bodyweight Reverse Hyperextension", "3", "10-20", 0xFFff60c4),
            new SCModel("Side Lying Clam", "1", "15-30 es", 0xFFf74cb8),
            new SCModel("Swiss Ball Crunch", "1", "15-30", 0xFFf42c6b),
            new SCModel("Swiss Ball Side Crunch", "1", "15-30 sec", 0xFFff0c3d)

    ));

    public static final ArrayList<SCModel> getWeekTwoC = new ArrayList<>(Arrays.asList(
            new SCModel("EXERCISE", "SETS", "REPS", 0xFFf45942),
            new SCModel("Bodyweight Pause Rep Hip Thrust", "3", "10-20", 0xFFFFBB00),
            new SCModel("Modified Inverted Row", "3", "8-12", 0xFFFFBB00),
            new SCModel("Goblet Squat", "3", "10-20", 0xFF609dff),
            new SCModel("Close Grip Barbell Bench Press", "3", "8-12", 0xFF609dff),
            new SCModel("Russian Kettlebell Swing", "3", "10-20", 0xFFff60c4),
            new SCModel("X-Band Walk (Moderate)", "1", "15-30 es", 0xFFf74cb8),
            new SCModel("Straight Leg Sit Up", "1", "15-30", 0xFFf42c6b),
            new SCModel("Band Rotary Hold", "1", "10-20 sec es", 0xFFff0c3d)

    ));

    public static final ArrayList<SCModel> getWeekThreeA = new ArrayList<>(Arrays.asList(
            new SCModel("EXERCISE", "SETS", "REPS", 0xFFf45942),
            new SCModel("Barbell Hip Thrust", "3", "10-20", 0xFFFFBB00),
            new SCModel("Dumbbell Bent Over Row", "3", "8-12", 0xFFFFBB00),
            new SCModel("Barbell Box Squat", "3", "10-20", 0xFF609dff),
            new SCModel("Push Up", "3", "8-12", 0xFF609dff),
            new SCModel("Barbell American Deadlift", "3", "10-20", 0xFFff60c4),
            new SCModel("Side Lying Abduction", "1", "15-30 es", 0xFFf74cb8),
            new SCModel("Dumbbell Swiss Ball Crunch", "1", "15-30", 0xFFf42c6b),
            new SCModel("Half Kneeling Cable Press", "1", "15-30 es", 0xFFff0c3d)

    ));


    public static final ArrayList<SCModel> getWeekThreeB = new ArrayList<>(Arrays.asList(
            new SCModel("EXERCISE", "SETS", "REPS", 0xFFf45942),
            new SCModel("Bodyweight Single Leg Hip Thrust", "3", "10-20 es", 0xFFFFBB00),
            new SCModel("Chin Up (Band Assisted)", "3", "1-5", 0xFFFFBB00),
            new SCModel("Bodyweight Bulgarian Split Squat", "3", "10-20 es", 0xFF609dff),
            new SCModel("Barbell Military Press", "3", "8-12", 0xFF609dff),
            new SCModel("Good Morning", "3", "10-20", 0xFFff60c4),
            new SCModel("X-Band Walk", "1", "15-30 es", 0xFFf74cb8),
            new SCModel("Feet Elevated Plank", "1", "60-120 sec", 0xFFf42c6b),
            new SCModel("Dumbbell Side Bend", "1", "15-30 es", 0xFFff0c3d)

    ));

    public static final ArrayList<SCModel> getWeekThreeC = new ArrayList<>(Arrays.asList(
            new SCModel("EXERCISE", "SETS", "REPS", 0xFFf45942),
            new SCModel("Barbell Pause Rep Hip Thrust", "3", "8-15", 0xFFFFBB00),
            new SCModel("Dumbbell Chest Supported Row", "3", "8-12", 0xFFFFBB00),
            new SCModel("Barbell Parallell Squat", "3", "10-15", 0xFF609dff),
            new SCModel("Barbell Incline Press", "3", "8-12", 0xFF609dff),
            new SCModel("Bodyweight Back Extension", "3", "10-20", 0xFFff60c4),
            new SCModel("Side Lying Clam", "1", "15-30 es", 0xFFf74cb8),
            new SCModel("Hanging Leg Raise", "1", "10-20", 0xFFf42c6b),
            new SCModel("Rope Horizontal Chop", "1", "10-15 es", 0xFFff0c3d)

    ));

}
