package soram.fitnessroutines.TDEE;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toolbar;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import soram.fitnessroutines.Calc.MetricConverter;
import soram.fitnessroutines.Data.MainData;
import soram.fitnessroutines.R;


public class TDEEFragment extends Fragment {
    View view;
    @BindView(R.id.calculatedTDEE) TextView tdee;
    @BindView(R.id.bodyWeight) EditText weight;
    @BindView(R.id.bodyHeight) EditText height;
    @BindView(R.id.age) EditText age;
    @BindView(R.id.calculateTDEEbutton) Button calculateButton;
    @BindView(R.id.sex2) ImageView maleSex;
    @BindView(R.id.sex1) ImageView femaleSex;
    @BindView(R.id.sitIcon) ImageView sitIcon;
    @BindView(R.id.walkIcon) ImageView walkIcon;
    @BindView(R.id.middleIcon) ImageView middleIcon;
    @BindView(R.id.proIcon) ImageView proIcon;
    @BindView(R.id.activityInfo) TextView activityTextView;
    @BindView(R.id.onerepmaxView) TextView oneRepMaxTextView;
    @BindView(R.id.textKg) TextView textKg;
    @BindView(R.id.textLbs) TextView textLbs;
    @BindView(R.id.switchMetric) SwitchCompat aSwitch;
    @BindView(R.id.bodyHeightImp2) EditText weightImp2;
    @BindView(R.id.bodyHeightImp) EditText weightImp;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> activityList = new ArrayList<>();
    String sex = "0";
    String activity = "0";
    Boolean metric = false;
//    @BindView(R.id.onerepmaxView) TextView oneRepMaxTextView;
//    @BindView(R.id.textKg) TextView textKg;
//    @BindView(R.id.textLbs) TextView textLbs;
////    @BindView(R.id.switchMetric) Switch aSwitch;
//    @BindView(R.id.bodyHeightImp) EditText weightImp;
//    @BindView(R.id.bodyHeightImp2) EditText weightImp2;




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tdee_layout, container, false);
//        tdee = view.findViewById(R.id.calculatedTDEE);
//        weight = view.findViewById(R.id.bodyWeight);

        setToolbar();

        ButterKnife.bind(this, view);



        activityList = MainData.activityLevel;
        activityTextView.setText(activityList.get(0));


        aSwitch.setOnCheckedChangeListener((compoundButton, b) -> {

            metric = b;
            if (b){
                height.setVisibility(View.INVISIBLE);
                weightImp.setVisibility(View.VISIBLE);
                weightImp2.setVisibility(View.VISIBLE);
            }else{
                height.setVisibility(View.VISIBLE);
                weightImp.setVisibility(View.INVISIBLE);
                weightImp2.setVisibility(View.INVISIBLE);
            }

        }  );


                maleSex.setOnClickListener(view1 -> {
                    maleSex.setImageResource(R.mipmap.male_in);
                    femaleSex.setImageResource(R.mipmap.female_un);
                    sex = "1";

                });
        femaleSex.setOnClickListener(view1 -> {
            femaleSex.setImageResource(R.mipmap.female_in);
            maleSex.setImageResource(R.mipmap.male_un);
            sex = "0";
        });
        sitIcon.setOnClickListener(view1 -> {
            activity = "0";
            sitIcon.setImageResource(R.mipmap.sit_in);
            walkIcon.setImageResource(R.mipmap.walk_un);
            middleIcon.setImageResource(R.mipmap.middle_un);
            proIcon.setImageResource(R.mipmap.pro_un);
            activityTextView.setText(activityList.get(0));
        });
        walkIcon.setOnClickListener(view1 -> {
            activity = "1";
            sitIcon.setImageResource(R.mipmap.sit_un);
            walkIcon.setImageResource(R.mipmap.walk_in);
            middleIcon.setImageResource(R.mipmap.middle_un);
            proIcon.setImageResource(R.mipmap.pro_un);
            activityTextView.setText(activityList.get(1));
        });
        middleIcon.setOnClickListener(view1 -> {
            activity = "2";
            sitIcon.setImageResource(R.mipmap.sit_un);
            walkIcon.setImageResource(R.mipmap.walk_un);
            middleIcon.setImageResource(R.mipmap.middle_in);
            proIcon.setImageResource(R.mipmap.pro_un);
            activityTextView.setText(activityList.get(2));
        });
        proIcon.setOnClickListener(view1 -> {
            activity = "3";
            sitIcon.setImageResource(R.mipmap.sit_un);
            walkIcon.setImageResource(R.mipmap.walk_un);
            middleIcon.setImageResource(R.mipmap.middle_un);
            proIcon.setImageResource(R.mipmap.pro_in);
            activityTextView.setText(activityList.get(3));
        });



        calculateButton.setOnClickListener(view1 -> {

            try {
                if (metric){

                    arrayList = TDEECalculation.calculateTdee(MetricConverter.getMetricWeight(weight.getText().toString()), MetricConverter.getMetricHeight(weightImp.getText().toString(), weightImp2.getText().toString()), age.getText().toString(), activity, sex);

                }else {
                    arrayList = TDEECalculation.calculateTdee(weight.getText().toString(), height.getText().toString(), age.getText().toString(), activity, sex);
                }
                setViews();
                tdee.setText(arrayList.get(0));
            }catch (NumberFormatException n){
                n.printStackTrace();
            }


        });

        return view;
    }
    public void setViews(){
        oneRepMaxTextView.setVisibility(View.VISIBLE);
        textKg.setVisibility(View.INVISIBLE);
        textLbs.setVisibility(View.INVISIBLE);
        aSwitch.setVisibility(View.INVISIBLE);
    }


    public void setToolbar(){
        ImageView imageView = getActivity().findViewById(R.id.info);
        imageView.setVisibility(View.VISIBLE);
        imageView.setOnClickListener(view1 -> openInfo());
        TextView toolbar = getActivity().findViewById(R.id.toolbarText);
        toolbar.setText(getResources().getText(R.string.tdee));
    }

    public void openInfo(){
        int orange = getResources().getColor(R.color.orange);


        new MaterialDialog.Builder(getContext())
                .title("Info")
                .titleColor(orange)
                .positiveColor(orange)
                .titleGravity(GravityEnum.CENTER)
                .content(MainData.getTDEEInfo)
                .positiveText("Hide")

                .show();
    }


}
