package soram.fitnessroutines.TDEE;

import java.math.RoundingMode;
import java.text.DecimalFormat;


import java.util.ArrayList;

public class TDEECalculation {
    private static DecimalFormat df = new DecimalFormat("#");

    public static ArrayList<String> calculateTdee(String weight, String height,  String age, String activity, String sex){

        ArrayList<String> arrayList = new ArrayList<>();
        Double BMR;
        int activityInt = Integer.parseInt(activity);
        if (sex.equals("0")){

            BMR = 10* (toInt(weight)) + 6.25 * (toInt(height)) - 5*(toInt(age)) - 161;
//            BMR = (toInt(weight)* 6.25) + (toInt(height)* 9.99) - (toInt(age)*  4.92) - 161;
            arrayList.add(maleTDEE(BMR, activityInt));
        }else {
            BMR = 10* (toInt(weight)) + 6.25 * (toInt(height)) - 5*(toInt(age)) + 5;
//            BMR = (toInt(weight)* 6.25) + (toInt(height)*9.99) - (toInt(age)* 4.92) + 5;
            arrayList.add(maleTDEE(BMR, activityInt));
        }


        return arrayList;

    }

    public static String toStr(Double d){
        df.setRoundingMode(RoundingMode.DOWN);
        return df.format(d);
    }
    private static Integer toInt(String d){
        return Integer.parseInt(d);

    }
//    public static String femaleTDEE(Double d, int act){
//        int i = (int) Math.rint(d);
//        int tdee = 0;
//        switch (act){
//            case 0: tdee = (int) Math.rint(i * 1.1); break;
//            case 1: tdee = (int) Math.rint(i * 1.275); break;
//            case 2: tdee = (int) Math.rint(i * 1.35); break;
//            case 3: tdee = (int) Math.rint(i * 1.525); break;
//            case 4: tdee = (int) Math.rint(i * 1.7); break;
//        }
//        return String.valueOf(tdee);
//
//    }

    public static String maleTDEE(Double d, int act){
        int i = (int) Math.rint(d);
        int tdee = 0;
        switch (act){
            case 0: tdee = (int) Math.rint(i * 1.2); break;
            case 1: tdee = (int) Math.rint(i * 1.375); break;
            case 2: tdee = (int) Math.rint(i * 1.55); break;
            case 3: tdee = (int) Math.rint(i * 1.725); break;
            case 4: tdee = (int) Math.rint(i * 1.9); break;
        }
        return String.valueOf(tdee);

    }

    }

