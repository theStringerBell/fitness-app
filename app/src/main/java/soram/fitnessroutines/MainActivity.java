package soram.fitnessroutines;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import soram.fitnessroutines.Routines.ICF.IceCreamFragment;
import soram.fitnessroutines.Routines.PPL.PPLFragment;
import soram.fitnessroutines.Routines.RoutinesFragment;
import soram.fitnessroutines.Routines.SC.StrongCurvesFragment;
import soram.fitnessroutines.Routines.StrongLift.StringLiftfragment;

public class MainActivity extends AppCompatActivity {
    private Context mContext;
    public  Context getContext(){
        return mContext;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = getApplicationContext();
//        setShared();




        loadfragment(new MainFragment());



    }

    @Override
    public void onBackPressed() {


        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.rl);



        if (fragment instanceof MainFragment){
            new AlertDialog.Builder(this)
                    .setTitle("Do you really want to exit?")
                    .setNegativeButton(R.string.action_no, null)
                    .setPositiveButton(R.string.action_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.super.onBackPressed();
                            finish();
                        }
                    }).create().show();


        }
        else if(fragment instanceof StringLiftfragment || fragment instanceof StrongCurvesFragment || fragment instanceof IceCreamFragment || fragment instanceof PPLFragment){
            loadfragment(new RoutinesFragment());

        }



        else{
            loadfragment(new MainFragment());
        }




    }
    public void loadfragment(Fragment fragment){

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.rl, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }


}
