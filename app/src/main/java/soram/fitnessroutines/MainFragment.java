package soram.fitnessroutines;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import soram.fitnessroutines.Routines.RoutinesFragment;
import soram.fitnessroutines.TDEE.TDEEFragment;
import soram.fitnessroutines.TM.TMFragment;


public class MainFragment extends Fragment {
    @BindView(R.id.Routines)
    RelativeLayout routinesButton;
    @BindView(R.id.RMC)
    RelativeLayout tmButton;
    @BindView(R.id.TDEE)
    RelativeLayout tdeeButton;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment, container, false);
        ButterKnife.bind(this, view);


        setToolbar();
        routinesButton.setOnClickListener(view1 -> loadfragment(new RoutinesFragment()));
        tmButton.setOnClickListener(view1 -> loadfragment(new TMFragment()));
        tdeeButton.setOnClickListener(view1 -> loadfragment(new TDEEFragment()));

        return view;
    }
    public void loadfragment(Fragment fragment)throws NullPointerException{

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.rl, fragment);
        fragmentTransaction.commit();


    }
    public void setToolbar(){
        ImageView imageView = getActivity().findViewById(R.id.info);
        imageView.setVisibility(View.GONE);
        TextView toolbar = getActivity().findViewById(R.id.toolbarText);
        toolbar.setText(getResources().getText(R.string.menu));
    }

}

