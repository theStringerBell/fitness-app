package soram.fitnessroutines.TM;


import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class RepCalculation {
    private static DecimalFormat df = new DecimalFormat("#");

    public static ArrayList<String> ok(String weight, String reps){


        ArrayList<String> arrayList = new ArrayList<>();
        Double i = Double.parseDouble(weight);
        int r =Integer.parseInt(reps);
        Double oneRep = 100 * i/(101.3 - 2.67123*r);



        arrayList.add(toStr(oneRep));
        arrayList.add(toStr(0.95*oneRep));
        arrayList.add(toStr(0.9*oneRep));
        arrayList.add(toStr(0.85*oneRep));
        arrayList.add(toStr(0.8*oneRep));
        arrayList.add(toStr(0.75*oneRep));
        arrayList.add(toStr(0.70*oneRep));
        arrayList.add(toStr(0.65*oneRep));
        arrayList.add(toStr(0.6*oneRep));
        arrayList.add(toStr(0.55*oneRep));
        arrayList.add(toStr(0.50*oneRep));
        return arrayList;
    }

    public static String toStr(Double d){
        df.setRoundingMode(RoundingMode.DOWN);
        return df.format(d);
    }
}
