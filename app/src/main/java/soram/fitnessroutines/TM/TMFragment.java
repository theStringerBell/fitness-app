package soram.fitnessroutines.TM;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.ghyeok.stickyswitch.widget.StickySwitch;
import soram.fitnessroutines.Data.MainData;
import soram.fitnessroutines.R;


public class TMFragment extends Fragment {

    View view;
    @BindView(R.id.weight) EditText weight;
    @BindView(R.id.reps) EditText reps;
    @BindView(R.id.onerepmaxWeight) TextView oneRepMaxTextView;
    @BindView(R.id.calculate) Button calculateButton;
    ArrayList<String> arrayList = new ArrayList<>();
    @BindView(R.id.repsListView) ListView listView;
    @BindView(R.id.repsListView2) ListView listView2;
    ArrayList<ListModel> listModel;
    ArrayList<ListModel> listModel2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tm_fragment, container, false);

        ButterKnife.bind(this, view);
        setToolbar();


        calculateButton.setOnClickListener(view1 -> {
            try {
                arrayList = RepCalculation.ok(weight.getText().toString(), reps.getText().toString());
                ArrayList<String> repPercentages = new MainData().repPercentages();
                listModel = new ArrayList<>();
                listModel2 = new ArrayList<>();

                for (int i = 1; i < 6; i++){
                    listModel.add(new ListModel(repPercentages.get(i), arrayList.get(i)));
                }
                for (int i = 6; i < arrayList.size(); i++){
                    listModel2.add(new ListModel(repPercentages.get(i), arrayList.get(i)));
                }
                CustomAdapter customAdapter = new CustomAdapter(getContext(), R.layout.listview_layout, listModel);
                CustomAdapter customAdapter2 = new CustomAdapter(getContext(), R.layout.listview_layout, listModel2);
                listView.setAdapter(customAdapter);
                listView2.setAdapter(customAdapter2);

                oneRepMaxTextView.setText(arrayList.get(0));
            }catch (NumberFormatException n){
                n.printStackTrace();
            }

        });

        return view;
    }


    public void setToolbar(){
        TextView toolbar = getActivity().findViewById(R.id.toolbarText);
        toolbar.setText(getResources().getText(R.string.repMax));
    }

}
