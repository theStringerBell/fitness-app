package soram.fitnessroutines.TM;



public class ListModel {
    private String text;
    private String weight;
    public ListModel(String text, String weight){
        this.text = text;
        this.weight = weight;
    }

    public String getText() {
        return text;
    }

    public String getWeight() {
        return weight;
    }
}
