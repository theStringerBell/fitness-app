package soram.fitnessroutines.Calc;



public class MetricConverter {
    public static String getMetricHeight(String a, String b){
        int aa = Integer.parseInt(a);
        int bb = Integer.parseInt(b);
        Double result = (30.48 * aa) + (2.54 * bb);
        return String.valueOf((int) Math.rint(result));
    }
    public static String getMetricWeight(String a){
        int aa = Integer.parseInt(a);

        Double result = (aa / 2.2046 );
        return String.valueOf((int) Math.rint(result));
    }
}
