package soram.fitnessroutines.Calc;



public class AddWeight {

    public static String addTwo(float i){
        return String.valueOf(i + 2.5);
    }
    public static String addFive(float i){
        return String.valueOf(i + 5);
    }
    public static String addMin(float i){
        return String.valueOf(i + 1.25);
    }
}
