package soram.fitnessroutines.Routines;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import soram.fitnessroutines.Data.MainData;
import soram.fitnessroutines.R;
import soram.fitnessroutines.Routines.ICF.IceCreamFragment;
import soram.fitnessroutines.Routines.PPL.PPLFragment;
import soram.fitnessroutines.Routines.SC.StrongCurvesFragment;
import soram.fitnessroutines.Routines.StrongLift.StringLiftfragment;


public class RoutinesFragment extends Fragment {
    @BindView(R.id.routineList) ListView listView;

    RoutineAdapter routineAdapter;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.routines_fragment, container, false);

    ButterKnife.bind(this, view);

    routineAdapter = new RoutineAdapter(getContext(), R.layout.routine_listview, MainData.getRoutineList );
    setToolbar();

    listView.setAdapter(routineAdapter);

    listView.setOnItemClickListener((adapterView, view1, i, l) -> {
        switch (i) {
            case 0:
                StrongCurvesFragment sc = new StrongCurvesFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.rl, sc, "tag")
                        .commit(); break;
            case 1:
                StringLiftfragment ss = new StringLiftfragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.rl, ss, "tag")
                        .commit(); break;

            case 2:
                IceCreamFragment icf = new IceCreamFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.rl, icf, "tag")
                        .commit(); break;

            case 3:
                PPLFragment ppl= new PPLFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.rl, ppl, "tag")
                        .commit(); break;


        }

    });

        return view;
    }
    public void setToolbar(){
        ImageView imageView = getActivity().findViewById(R.id.info);
        imageView.setVisibility(View.GONE);
        TextView toolbar = getActivity().findViewById(R.id.toolbarText);
        toolbar.setText(getResources().getText(R.string.routines));
    }

}
