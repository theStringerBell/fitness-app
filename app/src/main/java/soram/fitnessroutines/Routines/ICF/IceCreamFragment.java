package soram.fitnessroutines.Routines.ICF;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SwitchCompat;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import soram.fitnessroutines.Data.MainData;
import soram.fitnessroutines.R;


public class IceCreamFragment extends Fragment {
    View view;

    @BindView(R.id.IcffirstLift)
    TextView firstLift;

    @BindView(R.id.IcfsecondLift)
    TextView secondtLift;

    @BindView(R.id.IcfthirdLift)
    TextView thirdLift;

    @BindView(R.id.IcffirstLift2)
    TextView firstLift2;

    @BindView(R.id.IcfsecondLift2)
    TextView secondtLift2;

    @BindView(R.id.IcfthirdLift2)
    TextView thirdLift2;

    @BindView(R.id.IcffourthLift2)
    TextView fourthLift2;

    @BindView(R.id.IcfnextWeek1)
    AppCompatButton nextButton;

    @BindView(R.id.IcfReset)
    AppCompatButton settings;

    @BindView(R.id.IcfrelativL)
    RelativeLayout relativeLayout;

    @BindView(R.id.IcfrelativL2)
    RelativeLayout relativeLayout2;

    @BindView(R.id.IcfweekA) TextView weekA;
    @BindView(R.id.IcfdayA) TextView dayA;
    @BindView(R.id.IcfdayC) TextView dayB;
    @BindView(R.id.IcfdayE) TextView dayC;
    @BindView(R.id.IcfweekA2) TextView weekB;
    @BindView(R.id.IcfdayA2) TextView dayA2;
    @BindView(R.id.IcfdayC2) TextView dayB2;
    @BindView(R.id.IcfdayE2) TextView dayC2;
    @BindView(R.id.IcfweekD2) TextView rest;

    TextView fourthLift;

    SharedPreferences sp;

    int week = 0;
    int textColor;
    int size;
    int orange;
    int day;
    boolean aOrB;

    float increase;
    float increaseSquat;
    boolean mOiIcf;


    float benchIcf;
    float squatIcf;
    float deadliftIcf;
    float rowIcf;
    float ohpresIcf;


    EditText eBench;
    EditText eSquat;
    EditText eDeadlift;
    EditText eRow;
    EditText eOhp;
    SwitchCompat eSwitch;




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ifc_fragment_layout, container, false);
        ButterKnife.bind(this, view);
        setWeek();
        setToolbar();



        settings.setOnClickListener(view1 -> openDialog());
        nextButton.setOnClickListener(view1 -> {
            if (!mOiIcf){
                increase = 5f;
                increaseSquat = 5f;

            }else {
                increase = 2.5f;
                increaseSquat = 2.5f;
            }


            squatIcf += increaseSquat;

            if (!aOrB){
                deadliftIcf += increase;
                relativeLayout.setVisibility(View.GONE);
                relativeLayout2.setVisibility(View.VISIBLE);
                aOrB = true;
            }else {
                rowIcf += increase;
                benchIcf += increase;
                ohpresIcf += increase;
                relativeLayout.setVisibility(View.VISIBLE);
                relativeLayout2.setVisibility(View.GONE);
                aOrB = false;
            }
            day+=1;
            setDay();
            setShared();
            setNumbers();

        });




        return view;
    }


    public void setShared() {
        sp = getContext().getSharedPreferences("Pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("WeekIcf", week);
        editor.putInt("DayIcf", day);
        editor.putBoolean("Metric", mOiIcf);
        editor.putBoolean("AorB", aOrB);
        editor.putFloat("BenchIcf", benchIcf);
        editor.putFloat("SquatIcf", squatIcf);
        editor.putFloat("DeadliftIcf", deadliftIcf);
        editor.putFloat("RowIcf", rowIcf);
        editor.putFloat("OhpIcf", ohpresIcf);
        editor.apply();
    }

    public void getShared() {
        sp = getContext().getSharedPreferences("Pref", Context.MODE_PRIVATE);
        week = sp.getInt("WeekIcf", 0);
        day = sp.getInt("DayIcf", 1);
        mOiIcf = sp.getBoolean("MetricIcf", false);
        aOrB = sp.getBoolean("AorB", true);
        squatIcf = sp.getFloat("SquatIcf", 0);
        benchIcf = sp.getFloat("BenchIcf", 0);
        deadliftIcf = sp.getFloat("DeadliftIcf", 0);
        rowIcf = sp.getFloat("RowIcf", 0);
        ohpresIcf = sp.getFloat("OhpIcf", 0);
        Log.d("dsa", String.valueOf(mOiIcf + " " + aOrB));


    }

    public void openDialog(){
        View view3 = LayoutInflater.from(getContext()).inflate(R.layout.icf_dialog_layout, null);

        eBench = view3.findViewById(R.id.icfBench);
        eSquat = view3.findViewById(R.id.icfSquat);
        eDeadlift = view3.findViewById(R.id.icfDeadlift);
        eRow = view3.findViewById(R.id.icfRow);
        eOhp = view3.findViewById(R.id.icfOhp);
        eSwitch = view3.findViewById(R.id.icfSwitch);

        eSwitch.setChecked(mOiIcf);
        eSwitch.setOnCheckedChangeListener((compoundButton, b) -> mOiIcf = b );

        new AlertDialog.Builder(getContext(), R.style.dialogTheme)
                .setView(view3)
                .setPositiveButton("Done", (dialogInterface, i) -> {

                            try {
                                if (!eSquat.getText().toString().isEmpty()){
                                    squatIcf = Float.parseFloat(eSquat.getText().toString());
                                }

                                if (!eBench.getText().toString().isEmpty()){
                                    benchIcf = Float.parseFloat(eBench.getText().toString());
                                }
                                if (!eDeadlift.getText().toString().isEmpty()){
                                    deadliftIcf = Float.parseFloat(eDeadlift.getText().toString());
                                }
                                if (!eRow.getText().toString().isEmpty()){
                                    rowIcf = Float.parseFloat(eRow.getText().toString());
                                }
                                if (!eOhp.getText().toString().isEmpty()){
                                    ohpresIcf = Float.parseFloat(eOhp.getText().toString());
                                }



                                setShared();
                                setNumbers();
                            }catch (NumberFormatException n){
                                n.printStackTrace();
                            }
                        }
                )
                .setNegativeButton("Cancel", (dialogInterface, i) -> {})
                .show();
    }


    public void setWeek() {
        orange = getResources().getColor(R.color.orange);
        textColor = rest.getCurrentTextColor();
        getShared();
        setDay();

        if (week == 0){ // first time open
            openDialog();
            week+=1;
        }
        else {
            setNumbers();
        }
    }

    public void setNumbers(){
        if (!mOiIcf){
            increase = 5f;

        }else {
            increase = 2.5f;
        }
        firstLift.setText(String.valueOf(squatIcf));
        secondtLift.setText(String.valueOf(benchIcf));
        thirdLift.setText(String.valueOf(rowIcf));

        firstLift2.setText(String.valueOf(squatIcf));
        secondtLift2.setText(String.valueOf(deadliftIcf));
        thirdLift2.setText(String.valueOf(ohpresIcf));
        fourthLift2.setText(String.valueOf(0.9f*rowIcf));

        if (!aOrB){
            relativeLayout.setVisibility(View.GONE);
            relativeLayout2.setVisibility(View.VISIBLE);

        }else {
            relativeLayout.setVisibility(View.VISIBLE);
            relativeLayout2.setVisibility(View.GONE);

        }
    }

    public void setDay(){
        orange = getResources().getColor(R.color.orange);
        textColor = rest.getCurrentTextColor();

        if (day == 1){
            weekB.setTextColor(textColor);
            weekA.setTextColor(orange);
            dayA.setTextColor(orange);
            dayC2.setTextColor(textColor);

        }
        if (day == 2){
            dayA.setTextColor(textColor);
            dayB.setTextColor(orange);
            weekA.setTextColor(orange);
        }
        if (day == 3){
            dayB.setTextColor(textColor);
            dayC.setTextColor(orange);
            weekA.setTextColor(orange);
        }
        if (day == 4){
            weekA.setTextColor(textColor);
            weekB.setTextColor(orange);
            dayA2.setTextColor(orange);
            dayC.setTextColor(textColor);
        }
        if (day == 5){
            dayA2.setTextColor(textColor);
            dayB2.setTextColor(orange);
            weekB.setTextColor(orange);
        }
        if (day == 6){
            dayB2.setTextColor(textColor);
            dayC2.setTextColor(orange);
            weekB.setTextColor(orange);
            day = 0;
        }
    }
    public void openInfo(){


        new MaterialDialog.Builder(getContext())
                .title("Info")
                .titleColor(orange)
                .positiveColor(orange)
                .titleGravity(GravityEnum.CENTER)
                .content(MainData.getIceCreamInfo)
                .positiveText("Hide")

                .show();
    }
    public void setToolbar() {
        ImageView imageView = getActivity().findViewById(R.id.info);
        imageView.setVisibility(View.VISIBLE);
        imageView.setOnClickListener(view1 -> openInfo());
        TextView toolbar = getActivity().findViewById(R.id.toolbarText);
        toolbar.setText(getResources().getText(R.string.ICF));
        orange = getResources().getColor(R.color.orange);
    }
}
