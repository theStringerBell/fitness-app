package soram.fitnessroutines.Routines;



public class RoutinesModel {
    private String name;
    private String difficulty;
    private String days;
    private int color;

    public RoutinesModel(String name, String difficulty, String days, int color){
        this.color = color;
        this.name = name;
        this.days = days;
        this.difficulty = difficulty;
    }

    public String getName() {
        return name;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public int getColor() {
        return color;
    }

    public String getDays() {
        return days;
    }
}
