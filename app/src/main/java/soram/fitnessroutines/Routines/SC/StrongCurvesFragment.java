package soram.fitnessroutines.Routines.SC;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.ramotion.foldingcell.FoldingCell;

import butterknife.BindView;
import butterknife.ButterKnife;
import soram.fitnessroutines.Data.MainData;
import soram.fitnessroutines.R;


public class StrongCurvesFragment extends Fragment {
    View view;
    @BindView(R.id.folding_cell)
    FoldingCell foldingCell;

    @BindView(R.id.folding_cell2)
    FoldingCell foldingCell2;

    @BindView(R.id.folding_cell3)
    FoldingCell foldingCell3;

    @BindView(R.id.tiles)
    NavigationTabStrip tiles;

    @BindView(R.id.tiles3)
    NavigationTabStrip tiles3;

    @BindView(R.id.tiles2)
    NavigationTabStrip tiles2;

    @BindView(R.id.scLL)
    ListView listView;

    @BindView(R.id.scLL2)
    ListView listView2;

    @BindView(R.id.scLL3)
    ListView listView3;

    @BindView(R.id.scRL)
    RelativeLayout rl;


    int orange;
    SCAdapter scAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.strong_curver_fragment, container, false);
        ButterKnife.bind(this, view);
        setToolbar();
        setWeekOneA();
        setWeekTwoA();
        setWeekThreeA();
        setTiles();


        foldingCell.setOnClickListener(view1 -> {
            foldingCell.toggle(false);
            foldingCell2.fold(false);
            foldingCell3.fold(false);
        });

        foldingCell2.setOnClickListener(view1 -> {
            foldingCell2.toggle(false);
            foldingCell.fold(false);
            foldingCell3.fold(false);

        });

        foldingCell3.setOnClickListener(view1 -> {
            foldingCell3.toggle(false);
            foldingCell.fold(false);
            foldingCell2.fold(false);

        });

        rl.setOnClickListener(view1 -> {
            foldingCell.fold(false);
            foldingCell2.fold(false);
            foldingCell3.fold(false);

        });

        return view;
    }

    public void setTiles(){
//        setWeekOneA();
        orange = getResources().getColor(R.color.orange);
        tiles.setTitles("Workout A", "Workout B", "Workout C");
        tiles.setTabIndex(0, true);
        tiles.setStripColor(orange);
        tiles2.setStripColor(orange);
        tiles3.setStripColor(orange);

        tiles2.setTitles("Workout A", "Workout B", "Workout C");
        tiles2.setTabIndex(0, true);
        tiles3.setTitles("Workout A", "Workout B", "Workout C");
        tiles3.setTabIndex(0, true);


        tiles.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

                switch (index){
                    case 0: setWeekOneA(); break;
                    case 1: setWeekOneB(); break;
                    case 2: setWeekOneC(); break;
                }
            }
            @Override
            public void onEndTabSelected(String title, int index) {
            }
        });

        tiles2.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

                switch (index){
                    case 0: setWeekTwoA(); break;
                    case 1: setWeekTwoB(); break;
                    case 2: setWeekTwoC(); break;
                }
            }
            @Override
            public void onEndTabSelected(String title, int index) {
            }
        });

        tiles3.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

                switch (index){
                    case 0: setWeekThreeA(); break;
                    case 1: setWeekThreeB(); break;
                    case 2: setWeekThreeC(); break;
                }
            }
            @Override
            public void onEndTabSelected(String title, int index) {
            }
        });
    }

    public void setWeekOneA(){
        scAdapter = new SCAdapter(getContext(), R.layout.sc_listview_layout, MainData.getWeekOneA);
        listView.setAdapter(scAdapter);

    }
    public void setWeekOneB(){
        scAdapter = new SCAdapter(getContext(), R.layout.sc_listview_layout, MainData.getWeekOneB);
        listView.setAdapter(scAdapter);

    }
    public void setWeekOneC(){
        scAdapter = new SCAdapter(getContext(), R.layout.sc_listview_layout, MainData.getWeekOneC);
        listView.setAdapter(scAdapter);

    }

    public void setWeekTwoA(){
        scAdapter = new SCAdapter(getContext(), R.layout.sc_listview_layout, MainData.getWeekTwoA);
        listView2.setAdapter(scAdapter);

    }
    public void setWeekTwoB(){
        scAdapter = new SCAdapter(getContext(), R.layout.sc_listview_layout, MainData.getWeekTwoB);
        listView2.setAdapter(scAdapter);

    }
    public void setWeekTwoC(){
        scAdapter = new SCAdapter(getContext(), R.layout.sc_listview_layout, MainData.getWeekTwoC);
        listView2.setAdapter(scAdapter);

    }

    public void setWeekThreeA(){
        scAdapter = new SCAdapter(getContext(), R.layout.sc_listview_layout, MainData.getWeekThreeA);
        listView3.setAdapter(scAdapter);

    }
    public void setWeekThreeB(){
        scAdapter = new SCAdapter(getContext(), R.layout.sc_listview_layout, MainData.getWeekThreeB);
        listView3.setAdapter(scAdapter);

    }
    public void setWeekThreeC(){
        scAdapter = new SCAdapter(getContext(), R.layout.sc_listview_layout, MainData.getWeekThreeC);
        listView3.setAdapter(scAdapter);

    }

    public void openInfo(){
        new MaterialDialog.Builder(getContext())
                .title("Info")
                .titleColor(orange)
                .positiveColor(orange)
                .titleGravity(GravityEnum.CENTER)
                .content(MainData.getStrongCurvesInfo)
                .positiveText("Hide")

                .show();
    }
    public void setToolbar() {
        ImageView imageView = getActivity().findViewById(R.id.info);
        imageView.setVisibility(View.VISIBLE);
        imageView.setOnClickListener(view1 -> openInfo());
        TextView toolbar = getActivity().findViewById(R.id.toolbarText);
        toolbar.setText(getResources().getText(R.string.sc));
        orange = getResources().getColor(R.color.orange);
    }
}
