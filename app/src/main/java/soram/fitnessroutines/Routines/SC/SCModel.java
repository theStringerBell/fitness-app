package soram.fitnessroutines.Routines.SC;


public class SCModel {

    private String name;
    private String sets;
    private String reps;
    private int color;

    public SCModel(String name, String sets,String reps, int color){
        this.color = color;
        this.name = name;
        this.sets = sets;
        this.reps = reps;
    }

    public String getName() {
        return name;
    }

    public String getSets() {
        return sets;
    }

    public String getReps() {
        return reps;
    }

    public int getColor() {
        return color;
    }
}
