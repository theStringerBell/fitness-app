package soram.fitnessroutines.Routines.SC;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import soram.fitnessroutines.R;

public class SCAdapter extends ArrayAdapter<SCModel>{
    private int res;


    public SCAdapter(@NonNull Context context, int resource, @NonNull List<SCModel> objects) {
        super(context, resource, objects);
        this.res = resource;

    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(res, null);
        }

        SCModel scModel = getItem(position);

        if (scModel != null){
            TextView name = view.findViewById(R.id.scTextll);
            TextView sets = view.findViewById(R.id.scTextll2);
            TextView reps = view.findViewById(R.id.scTextll3);

            if (name != null){
                name.setText(scModel.getName());
                name.setTextColor(scModel.getColor());

            }
            if (sets != null){
                sets.setText(scModel.getSets());
                sets.setTextColor(scModel.getColor());
            }
            if (reps != null){
                reps.setText(scModel.getReps());
                reps.setTextColor(scModel.getColor());
            }

        }


        return view;

    }
}
