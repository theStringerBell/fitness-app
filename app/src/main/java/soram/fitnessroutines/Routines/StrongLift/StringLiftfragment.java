package soram.fitnessroutines.Routines.StrongLift;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.zip.Inflater;
import butterknife.BindView;
import butterknife.ButterKnife;


import soram.fitnessroutines.Calc.AddWeight;

import soram.fitnessroutines.Data.MainData;
import soram.fitnessroutines.R;

public class StringLiftfragment extends Fragment {





    @BindView(R.id.squat)
    TextView squat;
    @BindView(R.id.bench)
    TextView bench;
    @BindView(R.id.row)
    TextView row;

    @BindView(R.id.squat2)
    TextView squat2;
    @BindView(R.id.bench2)
    TextView bench2;
    @BindView(R.id.row2)
    TextView row2;

    @BindView(R.id.squat3)
    TextView squat3;
    @BindView(R.id.bench3)
    TextView bench3;
    @BindView(R.id.row3)
    TextView row3;

    @BindView(R.id.firstLift)
    TextView firstLift;
    @BindView(R.id.secondLift)
    TextView secondtLift;
    @BindView(R.id.thirdLift)
    TextView thirdLift;

    @BindView(R.id.firstLift2)
    TextView firstLift2;
    @BindView(R.id.secondLift2)
    TextView secondtLift2;
    @BindView(R.id.thirdLift2)
    TextView thirdLift2;

    @BindView(R.id.firstLift3)
    TextView firstLift3;
    @BindView(R.id.secondLift3)
    TextView secondtLift3;
    @BindView(R.id.thirdLift3)
    TextView thirdLift3;

    @BindView(R.id.nextWeek1)
    AppCompatButton button;

    @BindView(R.id.gsreset) AppCompatButton settings;

    View view;
    View view2;
    ArrayList<String> arrayList = new ArrayList<>();
    SharedPreferences sp;
    int week = 0;
    int size;
    int orange;
    float smallInc;
    float medInc;
    int bigInc;
    boolean mOi;


    float benchS;
    float squatS;
    float deadlift;
    float chinup;
    float rowS;
    float ohpress;

    EditText eBench;
    EditText eSquat;
    EditText eDeadlift;
    EditText eRow;
    EditText eChinup;
    EditText eOhp;
    SwitchCompat eSwitch;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.stronglift_fragment, container, false);
        view2 = inflater.inflate(R.layout.gs_dialog_layout, null);
        ButterKnife.bind(this, view);

        eSwitch = view2.findViewById(R.id.gsSwitch);


        arrayList = MainData.getLiftNames; // "Bench press","Barbell row", "Chinup",  "Squat", "Overhead press",  "Deadlift"
        setToolbar();
        setWeek();

        settings.setOnClickListener(view1 -> openSettings());
        button.setOnClickListener(view1 -> {

            week += 1;
            if (mOi){
                smallInc = 1.25f;
                medInc = 2.5f;
                bigInc = 5;
            }else {
                smallInc = 2.5f;
                medInc = 5;
                bigInc = 10;
            }

            if (week % 2 != 0) {

                squatS += bigInc;
                benchS += smallInc;
                deadlift += medInc;
                chinup += smallInc;
                rowS += medInc;
                ohpress += medInc;
                setShared();
                weekOne();
            } else {

                squatS += bigInc;
                benchS += medInc;
                deadlift += medInc;
                chinup += medInc;
                rowS += smallInc;
                ohpress += smallInc;
                setShared();
                weekTwo();
            }
            setShared();
        });
        return view;

    }

    public void setToolbar() {
        ImageView imageView = getActivity().findViewById(R.id.info);
        imageView.setVisibility(View.VISIBLE);
        imageView.setOnClickListener(view1 -> openInfo());
        TextView toolbar = getActivity().findViewById(R.id.toolbarText);
        toolbar.setText(getResources().getText(R.string.ss));
        orange = getResources().getColor(R.color.orange);
    }

    public void weekOne() {
        setNumbers();
        squat.setText(arrayList.get(0));
        bench.setText(arrayList.get(2));
        row.setText(arrayList.get(3));

        squat2.setText(arrayList.get(4));
        bench2.setText(arrayList.get(1));
        row2.setText(arrayList.get(5));

        squat3.setText(arrayList.get(0));
        bench3.setText(arrayList.get(2));
        row3.setText(arrayList.get(3));



    }

    public void weekTwo() {
        setNumbers();
        squat.setText(arrayList.get(4));
        bench.setText(arrayList.get(1));
        row.setText(arrayList.get(3));

        squat2.setText(arrayList.get(0));
        bench2.setText(arrayList.get(2));
        row2.setText(arrayList.get(5));

        squat3.setText(arrayList.get(4));
        bench3.setText(arrayList.get(1));
        row3.setText(arrayList.get(3));
    }

    public void getShared() {
        sp = getContext().getSharedPreferences("Pref", Context.MODE_PRIVATE);
        week = sp.getInt("Week", 1);
        mOi = sp.getBoolean("Metric", false);
         squatS = sp.getFloat("Squat", 0);
         benchS = sp.getFloat("Bench", 0);
         deadlift = sp.getFloat("Deadlift", 0);
         chinup = sp.getFloat("Chinup", 0);
         rowS = sp.getFloat("Row", 0);
         ohpress = sp.getFloat("Ohp", 0);


    }

    public void setShared() {
        sp = getContext().getSharedPreferences("Pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("Week", week);
        editor.putBoolean("Metric", mOi);
        editor.putFloat("Bench", benchS);
        editor.putFloat("Squat", squatS);
        editor.putFloat("Deadlift", deadlift);
        editor.putFloat("Row", rowS);
        editor.putFloat("Chinup", chinup);
        editor.putFloat("Ohp", ohpress);
        editor.apply();
    }

    public void setWeek() {
        getShared();

        if (week % 2 == 0) {
            weekTwo();
        } else {
            weekOne();
        }
    }


    public void setNumbers(){
        if (week % 2 != 0){

            firstLift.setText(String.valueOf(benchS));
            secondtLift.setText(String.valueOf(chinup));
            thirdLift.setText(String.valueOf(squatS));

            firstLift2.setText(String.valueOf(ohpress));
            secondtLift2.setText(String.valueOf(rowS));
            thirdLift2.setText(String.valueOf(deadlift));
            if (mOi){
                firstLift3.setText(AddWeight.addMin(benchS));
                secondtLift3.setText(AddWeight.addMin(chinup));
                thirdLift3.setText(AddWeight.addTwo(squatS));
            }else{
                firstLift3.setText(AddWeight.addTwo(benchS));
                secondtLift3.setText(AddWeight.addTwo(chinup));
                thirdLift3.setText(AddWeight.addFive(squatS));
            }

        }else{
            firstLift.setText(String.valueOf(ohpress));
            secondtLift.setText(String.valueOf(rowS));
            thirdLift.setText(String.valueOf(squatS));

            firstLift2.setText(String.valueOf(benchS));
            secondtLift2.setText(String.valueOf(chinup));
            thirdLift2.setText(String.valueOf(deadlift));
            if (mOi){
                firstLift3.setText(AddWeight.addMin(ohpress));
                secondtLift3.setText(AddWeight.addMin(rowS));
                thirdLift3.setText(AddWeight.addTwo(squatS));

            }else {
                firstLift3.setText(AddWeight.addTwo(ohpress));
                secondtLift3.setText(AddWeight.addTwo(rowS));
                thirdLift3.setText(AddWeight.addFive(squatS));
            }
        }
    }

    public void openSettings(){
        View view3 = LayoutInflater.from(getContext()).inflate(R.layout.gs_dialog_layout, null);

        eBench = view3.findViewById(R.id.gsBench);
        eSquat = view3.findViewById(R.id.gsSquat);
        eDeadlift = view3.findViewById(R.id.gsDeadlift);
        eChinup = view3.findViewById(R.id.gsChinup);
        eRow = view3.findViewById(R.id.gsRow);
        eOhp = view3.findViewById(R.id.gsOhp);
        eSwitch = view3.findViewById(R.id.gsSwitch);

        eSwitch.setChecked(mOi);
        eSwitch.setOnCheckedChangeListener((compoundButton, b) -> mOi = b );



        new AlertDialog.Builder(getContext(), R.style.dialogTheme)
                .setView(view3)
                .setPositiveButton("Done", (dialogInterface, i) -> {

                            try {
                                if (!eSquat.getText().toString().isEmpty()){
                                    squatS = Float.parseFloat(eSquat.getText().toString());
                                }

                                if (!eBench.getText().toString().isEmpty()){
                                    benchS = Float.parseFloat(eBench.getText().toString());
                                }
                                if (!eDeadlift.getText().toString().isEmpty()){
                                    deadlift = Float.parseFloat(eDeadlift.getText().toString());
                                }
                                if (!eChinup.getText().toString().isEmpty()){
                                    chinup = Float.parseFloat(eChinup.getText().toString());
                                }
                                if (!eRow.getText().toString().isEmpty()){
                                    rowS = Float.parseFloat(eRow.getText().toString());
                                }
                                if (!eOhp.getText().toString().isEmpty()){
                                    ohpress = Float.parseFloat(eOhp.getText().toString());
                                }


                                setShared();
                                setWeek();
                            }catch (NumberFormatException n){
                                n.printStackTrace();
                            }
                        }
                )
                .setNegativeButton("Cancel", (dialogInterface, i) -> {})
                .show();

    }

    public void openInfo(){
        new MaterialDialog.Builder(getContext())
                .title("Info")
                .titleColor(orange)
                .positiveColor(orange)
                .titleGravity(GravityEnum.CENTER)
                .content(MainData.getGreySkullInfo)
                .positiveText("Hide")

                .show();
    }
}

