package soram.fitnessroutines.Routines.PPL;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ramotion.foldingcell.FoldingCell;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import soram.fitnessroutines.Data.MainData;
import soram.fitnessroutines.R;


public class PPLFragment extends Fragment {

    @BindView(R.id.pplnextWeek)
    AppCompatButton next;

    @BindView(R.id.pplreset)
    AppCompatButton settings;

    @BindView(R.id.pplFirst)
    TextView pushFirst;

    @BindView(R.id.pplFirst2)
    TextView pullFirst;

    @BindView(R.id.pplSecond)
    TextView pushSecond;

    @BindView(R.id.pplFirstLift)
    TextView pushFirstLift;

    @BindView(R.id.pplFirstLift2)
    TextView pullFirstLift;

    @BindView(R.id.pplFirstLift3)
    TextView legsFirstLift;

    @BindView(R.id.folding_cell)
    FoldingCell foldingCell;


    @BindView(R.id.folding_cell2)
    FoldingCell foldingCell2;


    @BindView(R.id.folding_cell3)
    FoldingCell foldingCell3;

    @BindView(R.id.pplFirstSet2)
    TextView pullSet;


    @BindView(R.id.pplRL)
    RelativeLayout rl;
    int orange;


    EditText eBench;
    EditText eSquat;
    EditText eDeadlift;
    EditText eRow;
    EditText eOhp;
    SwitchCompat eSwitch;



    View view;
    float bench;
    float row;
    float squat;
    float deadlift;
    float ohp;
    boolean mOi;
    SharedPreferences sp;
    int week;
    float smallInc;
    float medInc;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ppl_fragment, container, false);
        ButterKnife.bind(this, view);
        setToolbar();
        getShared();
        setWeek();

        foldingCell.setOnClickListener(view1 -> {
                    foldingCell.toggle(false);
                    foldingCell2.fold(false);
                    foldingCell3.fold(false);

                }
            );

        foldingCell2.setOnClickListener(view1 -> {
                    foldingCell2.toggle(false);
                    foldingCell.fold(false);
                    foldingCell3.fold(false);
                }
        );

        foldingCell3.setOnClickListener(view1 -> {
                    foldingCell3.toggle(false);
                    foldingCell2.fold(false);
                    foldingCell.fold(false);
                }
        );

        rl.setOnClickListener(view1 -> {
            foldingCell2.fold(false);
            foldingCell.fold(false);
            foldingCell3.fold(false);


                }
        );

        next.setOnClickListener(view1 -> {
            if (!mOi){
                smallInc = 5f;
                medInc = 10f;

            }else {
                smallInc = 2.5f;
                medInc = 5;
            }

            squat += smallInc;
            if (week == 0){
                ohp += smallInc;
                week =1;

                row += smallInc;

            }else {
                week = 0;
                bench += smallInc;
                deadlift += medInc;
            }
            setWeek();
            setShared();
        });

        settings.setOnClickListener(view1 -> openDialog());



        return view;
    }

    public void getShared() {
        sp = getContext().getSharedPreferences("Pref", Context.MODE_PRIVATE);
        week = sp.getInt("WeekPpl", 0);
        mOi = sp.getBoolean("Metric", false);
        squat = sp.getFloat("SquatPpl", 0);
        bench = sp.getFloat("BenchPpl", 0);
        deadlift = sp.getFloat("DeadliftPpl", 0);

        row = sp.getFloat("RowPpl", 0);
        ohp = sp.getFloat("OhpPpl", 0);


    }

    public void setShared() {
        sp = getContext().getSharedPreferences("Pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("WeekPpl", week);
        editor.putBoolean("Metric", mOi);
        editor.putFloat("BenchPpl", bench);
        editor.putFloat("SquatPpl", squat);
        editor.putFloat("DeadliftPpl", deadlift);
        editor.putFloat("RowPpl", row);

        editor.putFloat("OhpPpl", ohp);
        editor.apply();
    }

    public void setWeek(){

        if (week == 0){
            pushFirst.setText(getResources().getString(R.string.ohp));
            pullFirst.setText(getResources().getString(R.string.row));
            pullSet.setText(getResources().getString(R.string.pplsetsBench));
            pushSecond.setText(getResources().getString(R.string.bench));
            pushFirstLift.setText(String.valueOf(ohp));
            pullFirstLift.setText(String.valueOf(row));
            legsFirstLift.setText(String.valueOf(squat));

        }else{
            pushFirst.setText(getResources().getString(R.string.bench));
            pullFirst.setText(getResources().getString(R.string.deadlift));
            pullSet.setText(getResources().getString(R.string.pplsets5));
            pushSecond.setText(getResources().getString(R.string.ohp));
            pushFirstLift.setText(String.valueOf(bench));
            legsFirstLift.setText(String.valueOf(squat));
            pullFirstLift.setText(String.valueOf(deadlift));

        }

    }

    public void openInfo(){

        new MaterialDialog.Builder(getContext())
                .title("Info")
                .titleColor(orange)
                .positiveColor(orange)
                .titleGravity(GravityEnum.CENTER)
                .content(MainData.getPPLInfo)
                .positiveText("Hide")

                .show();
    }

    public void setToolbar() {
        ImageView imageView = getActivity().findViewById(R.id.info);
        imageView.setVisibility(View.VISIBLE);
        imageView.setOnClickListener(view1 -> openInfo());
        TextView toolbar = getActivity().findViewById(R.id.toolbarText);
        toolbar.setText(getResources().getText(R.string.ppl));
        orange = getResources().getColor(R.color.orange);
    }

    public void openDialog(){
        View view3 = LayoutInflater.from(getContext()).inflate(R.layout.icf_dialog_layout, null);

        eBench = view3.findViewById(R.id.icfBench);
        eSquat = view3.findViewById(R.id.icfSquat);
        eDeadlift = view3.findViewById(R.id.icfDeadlift);
        eRow = view3.findViewById(R.id.icfRow);
        eOhp = view3.findViewById(R.id.icfOhp);
        eSwitch = view3.findViewById(R.id.icfSwitch);

        eSwitch.setChecked(mOi);
        eSwitch.setOnCheckedChangeListener((compoundButton, b) -> mOi = b );

        new AlertDialog.Builder(getContext(), R.style.dialogTheme)
                .setView(view3)
                .setPositiveButton("Done", (dialogInterface, i) -> {

                            try {
                                if (!eSquat.getText().toString().isEmpty()){
                                    squat = Float.parseFloat(eSquat.getText().toString());
                                }

                                if (!eBench.getText().toString().isEmpty()){
                                    bench = Float.parseFloat(eBench.getText().toString());
                                }
                                if (!eDeadlift.getText().toString().isEmpty()){
                                    deadlift = Float.parseFloat(eDeadlift.getText().toString());
                                }
                                if (!eRow.getText().toString().isEmpty()){
                                    row = Float.parseFloat(eRow.getText().toString());
                                }
                                if (!eOhp.getText().toString().isEmpty()){
                                    ohp = Float.parseFloat(eOhp.getText().toString());
                                }



                                setShared();
                                setWeek();
                            }catch (NumberFormatException n){
                                n.printStackTrace();
                            }
                        }
                )
                .setNegativeButton("Cancel", (dialogInterface, i) -> {})
                .show();
    }
}
