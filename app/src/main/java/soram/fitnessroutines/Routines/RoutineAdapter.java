package soram.fitnessroutines.Routines;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import soram.fitnessroutines.R;


public class RoutineAdapter extends ArrayAdapter<RoutinesModel>{
    private int res;


    public RoutineAdapter(@NonNull Context context, int resource, @NonNull List<RoutinesModel> objects) {
        super(context, resource, objects);
        this.res = resource;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(res, null);
        }

        RoutinesModel routinesModel = getItem(position);

        if (routinesModel != null){
            TextView name = view.findViewById(R.id.routineList);
            TextView description = view.findViewById(R.id.routineList2);
            TextView days = view.findViewById(R.id.routineList3);

            if (name != null){
                name.setText(routinesModel.getName());
            }
            if (days != null){
                days.setText(routinesModel.getDays());
            }
            if (description != null){
                description.setText(routinesModel.getDifficulty());
                description.setTextColor(routinesModel.getColor());
            }

        }



        return view;
    }
}
